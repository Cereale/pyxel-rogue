from constants import WINDOW_SIZE
from random import random, randint


def mid(a, b, c):
    return max(min(a, b), min(max(a, b), c))


def center(obj):
    x, y, w, h = obj.x, obj.y, obj.h, obj.w
    return [x + h / 2, y + w / 2]


def distance(r1, r2):
    return ((r1[0] - r2[0])**2 + (r1[1] - r2[1])**2)**(0.5)


def distance_2(x1, y1, x2, y2):
    return ((x1 - x2)**2 + (y1 - y2)**2)**0.5


def distance_objects(obj1, obj2):
    x1, y1 = center(obj1)
    x2, y2 = center(obj2)
    return distance_2(x1, y1, x2, y2)


def luminosity(distance, radius):  # From 0 to 1
    out = 0
    jitter = 0
    if randint(0, 10) == 0:
        jitter = 0.004 * random()
    out = abs((distance / radius + jitter)**1.2)
    out = min(1, out)
    return 1 - out


def lum_dist(x1, y1, x2, y2, radius, Print=False):
    if Print:
        print(x1, " ", y1, " ", x2, " ", y2, " ", distance_2(x1, y1, x2, y2))
    return luminosity(distance_2(x1, y1, x2, y2), radius)


def obj_distance(obj1, obj2):
    return distance([*center(obj1)], [*center(obj2)])


def linspace(x1, x2, nb):
    out = []
    dx = abs(x2 - x1) / nb
    for i in range(nb):
        out.append(x1 + i * dx)
    return out


def zeros(n):
    return linspace(0, 0, n)


def saw_animation(time, loop_time, frame_coordinates):
    n = len(frame_coordinates)
    dx = loop_time / (2 * n)
    print(loop_time, n, dx)
    animation_frame = abs((time - loop_time / 2) % loop_time - loop_time / 2)

    for i in range(n):
        if i * dx <= animation_frame < (i + 1) * dx:
            return frame_coordinates[i]
    return -100, -100


def make_2D_array(n=WINDOW_SIZE, p=WINDOW_SIZE):
    array = []
    for i in range(n):
        array.append([])
    for i in range(n):
        for j in range(p):
            array[i].append(0)
    return array


def reset_2D_array(array, n=WINDOW_SIZE, p=WINDOW_SIZE):
    for i in range(n):
        for j in range(p):
            array[i][j] = 0


def make_object_array(n, p):
    array = []
    for i in range(n):
        array.append([])
    for i in range(n):
        for j in range(p):
            array[i].append([])
    return array
