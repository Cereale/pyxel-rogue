import pyxel
from constants import WINDOW_SIZE, DEBUGGING
from utilities import make_object_array, center, obj_distance, linspace, zeros
from random import randint, random
from math import cos, sin, pi


def are_recangles_overlapping(xmin1, xmax1, ymin1, ymax1,
                              xmin2, xmax2, ymin2, ymax2):
    x_collide = xmax1 > xmin2 and xmin1 < xmax2
    y_collide = ymax1 > ymin2 and ymin1 < ymax2
    return x_collide and y_collide


def objects_overlapping(obj1, obj2):
    return are_recangles_overlapping(obj1.x + obj1.hitbox[0],
                                     obj1.x + obj1.hitbox[2],
                                     obj1.y + obj1.hitbox[1],
                                     obj1.y + obj1.hitbox[3],
                                     obj2.x + obj2.hitbox[0],
                                     obj2.x + obj2.hitbox[2],
                                     obj2.y + obj2.hitbox[1],
                                     obj2.y + obj2.hitbox[3])


# Level

class Level:
    def __init__(self, n, rooms, doors, walls):
        self.size = n
        self.layout = rooms
        self.doors = doors
        self.room = self.initiate_rooms(doors, walls)

    def initiate_rooms(self, doors, walls):
        rooms = make_object_array(self.size, self.size)
        for i in range(self.size):
            for j in range(self.size):
                rooms[i][j] = Room(i, j, doors, walls[i][j])
        return rooms


class Room:
    def __init__(self, i, j, doors, walls):
        self.i_room = i
        self.j_room = j
        self.doors = doors
        self.walls = walls
        self.cleared = 0
        self.active = 0  # Player inside
        self.monsters = []
        self.objects = []  # Torch(50, 50)


# Tiles

class Wall_tile:
    def __init__(self, x, y, orientation):
        self.x = x
        self.y = y
        self.w = 8
        self.h = 8
        self.hitbox = [0, 0, self.w, self.h]
        self.orientation = orientation  # [t, r, b, l] = [0,1,2,3]
        self.type = "wall"
        self.light_source = False
        self.L_radius = 0

    def draw(self):
        sum = self.orientation[0] + self.orientation[1] + \
              self.orientation[2] + self.orientation[3]
        if self.orientation[1] and sum == 1:
            pyxel.blt(self.x, self.y, 0, 0, 40, -self.w, self.h)
        elif self.orientation[3] and sum == 1:
            pyxel.blt(self.x, self.y, 0, 0, 40, self.w, self.h)
        elif self.orientation[0]:
            if self.orientation[1]:
                pyxel.blt(self.x, self.y, 0, 0, 32, -self.w, self.h)
            elif self.orientation[3]:
                pyxel.blt(self.x, self.y, 0, 0, 32, self.w, self.h)
            else:
                pyxel.blt(self.x, self.y, 0, 8, 32, self.w, self.h)
        elif self.orientation[2]:
            if self.orientation[1]:
                pyxel.blt(self.x, self.y, 0, 0, 32, -self.w, self.h)
            elif self.orientation[3]:
                pyxel.blt(self.x, self.y, 0, 0, 32, self.w, -self.h)
            else:
                pyxel.blt(self.x, self.y, 0, 8, 32, self.w, -self.h)


class Door_tile:
    def __init__(self, x, y, orientation, number):
        self.x = x
        self.y = y
        self.w = 8
        self.h = 8
        self.type = "door"
        self.hitbox = [0, 0, self.w, self.h]
        self.orientation = orientation  # [t, r, b, l] = [0,1,2,3]
        self.number = number

    def draw(self):
        if self.orientation == 0:
            if self.number == 0:
                pyxel.blt(self.x, self.y, 0, 16, 32, self.w, self.h, 11)
            elif self.number == 3:
                pyxel.blt(self.x, self.y, 0, 16, 32, -self.w, self.h, 11)
        if self.orientation == 1:
            if self.number == 0:
                pyxel.blt(self.x, self.y, 0, 16, 40, -self.w, -self.h, 11)
            elif self.number == 3:
                pyxel.blt(self.x, self.y, 0, 16, 40, -self.w, self.h, 11)
        if self.orientation == 2:
            if self.number == 0:
                pyxel.blt(self.x, self.y, 0, 16, 32, self.w, -self.h, 11)
            elif self.number == 3:
                pyxel.blt(self.x, self.y, 0, 16, 32, -self.w, -self.h, 11)
        if self.orientation == 3:
            if self.number == 0:
                pyxel.blt(self.x, self.y, 0, 16, 40, self.w, -self.h, 11)
            elif self.number == 3:
                pyxel.blt(self.x, self.y, 0, 16, 40, self.w, self.h, 11)


class Torch:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.w = 6
        self.h = 6
        self.light_source = True
        self.L_radius = 10

    def update(self):
        pass
        # if randint(0, 2):
        #     self.L_radius += random() - 0.8
        #     self.L_radius = mid(9, self.L_radius, 11)

    def draw(self):
        pyxel.blt(self.x + 1, self.y + 1, 0, 24, 56, 6, 6, 11)
        for i in range(randint(10, 15)):
            x, y = center(self)
            r = random() * 3
            theta = randint(0, 360)
            pyxel.pset(x + r * cos(theta), y + r * sin(theta), randint(8, 10))


# Projectile

class Fireball:
    def __init__(self, player, attack_dir):
        self.x = player.x + player.w / 2 - 1
        self.y = player.y + player.h / 2 - 1
        self.vx, self.vy = 0, 0
        if attack_dir == 0:
            self.vy = -1 * player.proj_v
        if attack_dir == 1:
            self.vx = player.proj_v
        if attack_dir == 2:
            self.vy = player.proj_v
        if attack_dir == 3:
            self.vx = -1 * player.proj_v
        self.name = "fireball"
        self.damage = 1
        self.active = 1
        self.w = 2
        self.h = 2
        self.hitbox = [0, 0, self.w, self.h]
        self.light_source = True
        self.L_radius = 8

    def update(self, room):
        walls = room.walls
        # monsters = room.monsters
        self.x += self.vx
        self.y += self.vy
        for wall in walls:
            if (wall.type == "wall" and objects_overlapping(self, wall) or
               (self.x < 0 - self.w or self.x > WINDOW_SIZE or
               self.y < 0 - self.h or self.y > WINDOW_SIZE)):
                self.active = 0

    def draw(self, time):
        if DEBUGGING:
            pyxel.rectb(self.x, self.y, self.w, self.h, 3)
        if (time % 6) < 3:
            pyxel.blt(self.x, self.y, 1, 48, 0, 4, 4, 11)
            self.L_radius = 9
        elif (time % 6) < 6:
            pyxel.blt(self.x, self.y, 1, 56, 0, 4, 4, 11)
            self.L_radius = 8


class Wave_particle:
    def __init__(self, x, y, vx, vy):
        self.x = x
        self.y = y
        self.w = 1
        self.h = 1
        self.hitbox = [0, 0, self.w, self.h]
        self.vx = vx
        self.vy = vy
        self.life_time = randint(20, 25)
        self.n = [0, 0]
        self.damage = 1 / 50
        self.active = 1
        self.light_source = False
        self.L_radius = 0

    def update(self, room):
        walls = room.walls
        monsters = room.monsters
        self.life_time -= 1
        self.x += self.vx
        self.y += self.vy
        collided = 0
        for wall in walls:
            if wall.type == "wall":
                xmin, xmax = wall.x, wall.x + wall.w
                # ymin, ymax = wall.y - 1, wall.y + wall.h + 1

                if objects_overlapping(self, wall) and collided == 0:
                    if (self.x < xmax - 2 and self.x > xmin + 2):
                        # self.y = wall.y + wall.h
                        self.vx *= -1
                        # self.vy *= -1
                    else:
                        # if (self.y < ymax and self.y > ymin):
                        # self.y = wall.y - self.h
                        self.vy *= -1
                        # self.vx *= -1
                    collided = 1

        for monster in monsters:
            if (monster.radius * 0.8 < obj_distance(self, monster) <
                    1.4 * monster.radius and monster.status == "attack"):
                mx, my = center(monster)
                # theta = atan2(my - self.y, mx - self.x)
                # self.x = mx + monster.radius * cos(theta)
                # self.y = my - monster.radius * sin(theta)
                self.n = [mx - self.x, my - self.y]
                vn = self.vx * self.n[0] + self.vy * self.n[1]
                nn = (self.n[0]**2 + self.n[1]**2)
                dvx = -2 * (vn / nn) * self.n[0]
                dvy = -2 * (vn / nn) * self.n[1]
                self.vx += dvx
                self.vy += dvy
                if self.active and monster.status == "attack":
                    self.active = 0
                    monster.hp -= self.damage
                    self.life_time = self.life_time // 2
                    monster.vx -= self.vx / 60
                    monster.vy -= self.vy / 60

        if (self.x + self.w > WINDOW_SIZE or self.y + self.h > WINDOW_SIZE or
                self.x < 0 or self.y < 0 or self.life_time < 0):
            return 1
        else:
            return 0

    def draw(self):
        pyxel.pset(self.x, self.y, 7)


class Sound_wave:
    def __init__(self, player, dir, speed):
        px, py, ph, pw = player.x, player.y, player.h, player.w
        px, py = px + pw / 2 - 1, py + ph / 2 - 1
        self.wave_particles = []
        self.active = 1
        self.name = 'soundwave'
        self.n_part = 30  # 30
        self.light_source = False
        self.L_radius = 0
        r = 5
        delta = pi / 6.5  # /8
        boost_speed = 1.2

        if dir == 2:
            theta = linspace(pi / 4 + delta,
                             3 * pi / 4 - delta, self.n_part)
        elif dir == 1:
            theta = linspace(7 * pi / 4 + delta,
                             9 * pi / 4 - delta, self.n_part)
        elif dir == 0:
            theta = linspace(5 * pi / 4 + delta,
                             7 * pi / 4 - delta, self.n_part)
        else:
            theta = linspace(3 * pi / 4 + delta,
                             5 * pi / 4 - delta, self.n_part)
        for i in range(self.n_part):
            t = theta[i]
            w_particle = Wave_particle(
                px + r * cos(t), py + r * sin(t),
                boost_speed * speed * cos(t), boost_speed * speed * sin(t))
            self.wave_particles.append(w_particle)

    def update(self, room):
        killed_part = zeros(self.n_part)
        for i, part in enumerate(self.wave_particles):
            killed_part[i] = part.update(room)
        for i in range(self.n_part - 1, -1, -1):
            if killed_part[i] == 1:
                self.wave_particles.pop(i)

    def draw(self, time):
        for part in self.wave_particles:
            part.draw()


# Sensors

class Sensor:
    def __init__(self, x, y, w, h):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.hitbox = [0, 0, self.w, self.h]
        self.color = randint(0, 15)

    def draw(self):
        pyxel.rect(self.x, self.y, self.w, self.h, self.color)


def make_sensors(parent_object, offset_x, offset_y):
    x = parent_object.x
    y = parent_object.y
    w = parent_object.w
    h = parent_object.h
    t_sensor = Sensor(x + offset_x, y, w - 2 * offset_x, h / 2)
    r_sensor = Sensor(x + w / 2, y + offset_y, w / 2, h - 2 * offset_y)
    b_sensor = Sensor(x + offset_x, y + h / 2, w - 2 * offset_x, h / 2)
    l_sensor = Sensor(x, y + offset_y, w / 2, h - 2 * offset_y)
    parent_object.sensors = [t_sensor, r_sensor, b_sensor, l_sensor]


def update_sensors(object, offset_x, offset_y):
    x = object.x
    y = object.y
    w = object.w
    h = object.h
    object.sensors[0].x = x + offset_x
    object.sensors[0].y = y
    object.sensors[1].x = x + w / 2
    object.sensors[1].y = y + offset_y
    object.sensors[2].x = x + offset_x
    object.sensors[2].y = y + h / 2
    object.sensors[3].x = x
    object.sensors[3].y = y + offset_y


def sensor_sensor(sens1, sens2):
    if objects_overlapping(sens1[0], sens2[2]):
        return 0
    elif objects_overlapping(sens1[2], sens2[0]):
        return 2
    elif objects_overlapping(sens1[1], sens2[3]):
        return 1
    elif objects_overlapping(sens1[3], sens2[1]):
        return 3
    else:
        return -1


def sensor_object(sens1, obj):
    if objects_overlapping(sens1[0], obj):
        return 0
    elif objects_overlapping(sens1[2], obj):
        return 2
    elif objects_overlapping(sens1[1], obj):
        return 1
    elif objects_overlapping(sens1[3], obj):
        return 3
    else:
        return -1


def wall_collision(obj, offset_x, offset_y, walls):
    update_sensors(obj, offset_x, offset_y)
    for wall in walls:
        if wall.type == "wall":
            if objects_overlapping(obj.sensors[0], wall):
                obj.y = wall.y + wall.h
                obj.vy = 0
            if objects_overlapping(obj.sensors[2], wall):
                obj.y = wall.y - obj.h
                obj.vy = 0
            if objects_overlapping(obj.sensors[1], wall):
                obj.x = wall.x - obj.w
                obj.vx = 0
            if objects_overlapping(obj.sensors[3], wall):
                obj.x = wall.x + wall.w
                obj.vx = 0
    update_sensors(obj, offset_x, offset_y)


# Player

class Player:
    def __init__(self):
        self.x = 80
        self.y = 80
        self.v = 3
        self.vx = 0
        self.vy = 0
        self.hp = 6
        self.max_hp = 6
        self.alive = True
        self.room_i, self.room_j = 0, 0
        self.dir = 2
        self.projectiles = []
        self.proj_v = 3.5
        self.w = 16
        self.h = 13

        self.hitbox = [0, 0, self.w, self.h]
        self.offset_x = 4
        self.offset_y = 4
        make_sensors(self, self.offset_x, self.offset_y)
        self.facing = 1
        self.kills = 0
        self.form = 1     # (0 = male), (1 = female), (2 = bat)
        self.status = "normal"
        self.shoot_cd = 0
        self.shoot_status = "ready"
        self.touched_cd = 0
        self.light_source = True
        self.L_radius = 40

    def update(self, time, KEYS, ZQSD_ASWD, level):

        # Cooldowns
        self.shoot_cd = max(0, self.shoot_cd - 1)
        self.touched_cd = max(0, self.touched_cd - 1)
        if self.shoot_cd == 0:
            self.shoot_status = "ready"
        if self.touched_cd == 0:
            self.status = "normal"
        # Which projectile
        if (self.form == 2):
            self.proj_name = "wave"
        else:
            self.proj_name = "fire"

        # Morph player:
        if pyxel.btnp(pyxel.KEY_SPACE):
            self.form = (self.form + 1) % 3
        #     if (self.form == 2):
        #         self.h = 8
        #     else:
        #         self.h = 13

        if self.hp < 1:
            print("GAME OVER")
            self.alive = 0

        old_vx = self.vx
        old_vy = self.vy

        # Move player
        if pyxel.btn(pyxel.KEY_LEFT):
            self.vx = old_vx - 1.7
            self.vx = max(self.vx, -self.v)
            self.facing = -1
            self.dir = 3
        if pyxel.btn(pyxel.KEY_RIGHT):
            self.vx = old_vx + 1.7
            self.vx = min(self.vx, self.v)
            self.dir = 1
            self.facing = 1
        if pyxel.btn(pyxel.KEY_UP):
            self.vy = old_vy - 1.7
            self.vy = max(self.vy, -self.v)
            self.dir = 0
        if pyxel.btn(pyxel.KEY_DOWN):
            self.vy = old_vy + 1.7
            self.vy = min(self.vy, +self.v)
            self.dir = 2

        self.vy *= 0.6
        self.vx *= 0.6

        self.x += self.vx
        self.y += self.vy

        wall_collision(self, self.offset_x, self.offset_y,
                       level.room[self.room_i][self.room_j].walls)

        # Shoot
        for i in range(4):
            if pyxel.btn(KEYS[ZQSD_ASWD][i]):  # right
                if self.shoot_cd == 0:
                    self.attack(i)
                if i == 1:
                    self.facing = 1
                if i == 3:
                    self.facing = -1

        # if randint(0, 2):
        #     self.L_radius += (random() - 0.5) * 1.5
        #     self.L_radius = mid(29, self.L_radius, 31)

    def heal(self, hp_healed):
        self.hp = min(self.hp + hp_healed, self.max_hp)

    def attack(self, attack_dir):
        self.shoot_status = "reloading"
        if self.proj_name == "wave":
            self.shoot_cd = 8
            proj = Sound_wave(self, attack_dir, self.proj_v)
        if self.proj_name == "fire":
            self.shoot_cd = 10
            proj = Fireball(self, attack_dir)

        self.projectiles.append(proj)

    def draw(self, time):
        if DEBUGGING:
            pyxel.text(self.x + self.w, self.y + self.h, self.status, 11)
            pyxel.text(self.x + self.w, self.y + self.h + 6,
                       self.shoot_status, 11)
            pyxel.pset(*center(self), 11)
            pyxel.circ(*center(self), (self.w + self.h) / 4, 11)
            for sensor in self.sensors:
                sensor.draw()
            # pyxel.rectb(self.x, self.y, self.w, self.h, 3)
        if self.alive is True:
            if (self.status == "normal" or
                    (self.status == "touched" and time % 7 < 5)):
                # print("HERE :::",x_coord, y_coord)
                t_local = (time / 15)
                frac = 1 / 8
                if (t_local % 1) < frac:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 0,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 2:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 16,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 3:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 32,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 4:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 48,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 5:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 64,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 6:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 48,
                              self.w * self.facing, self.h, 11)
                elif (t_local % 1) < frac * 7:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 32,
                              self.w * self.facing, self.h, 11)
                else:
                    pyxel.blt(self.x, self.y, 1, self.form * 16, 16,
                              self.w * self.facing, self.h, 11)


# Dummy

class Dummy:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.v = 1
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0
        self.hp = 4
        self.max_hp = 6
        self.alive = True
        self.dir = 2
        self.projectiles = []
        self.last_shot = 0
        self.cd = 10
        self.w = 64
        self.h = 64
        self.radius = (self.w + self.h) / 4
        self.hitbox = [0, 0, self.w, self.h]
        self.offset_x = 3
        self.offset_y = 3
        make_sensors(self, self.offset_x, self.offset_y)
        self.status = "attack"
        self.will_teleport = 0
        self.fadeout_cd = 0
        self.fadein_cd = 15
        self.pop_cd = 0
        self.light_source = False
        self.L_radius = 0

    def update(self, player, monsters, time, walls):
        pass

    def draw(self, time, player):
        # print(self.player.room_i, self.player.room_j, i)
        # pyxel.text(self.x+16, self.y+16, self.status + str(self.pop_cd), 8)
        # if (self.x - player.x) < 0:
        #     dir = 1
        # else:
        #     dir = -1
        if DEBUGGING:
            # pyxel.text(self.x + self.w, self.y + self.h, self.status, 11)
            pyxel.rectb(self.x, self.y, self.w, self.h, 3)
            pyxel.pset(*center(self), 11)
            pyxel.circb(*center(self), (self.w + self.h) / 4, 11)

        class Pixel:
            def __init__(self, x, y):
                self.x = x
                self.y = y
                self.h = 1
                self.w = 1
        for i in range(0, 160, 1):
            for j in range(0, 160, 1):
                pixel = Pixel(i, j)
                if (self.radius * 0.9 < obj_distance(self, pixel) <
                        self.radius):
                    pyxel.pset(i, j, 15)

    def mutual_collide(self, monster2):
        pass


class Monster:
    def __init__(self):
        self.x = 80
        self.y = 80
        self.v = 1
        self.hp = 0
        self.max_hp = 6
        self.alive = True
        self.dir = 2
        self.projectiles = []
        self.last_shot = 0
        self.cd = 10
        self.w = 0
        self.h = 0
        self.hitbox = [0, 0, self.w, self.h]


# Ghost

class Ghost:
    def __init__(self):
        self.x = randint(80, 80)
        self.y = randint(80, 80)
        self.v = 1
        self.vx = 0
        self.vy = 0
        self.ax = 0
        self.ay = 0
        self.hp = 4
        self.max_hp = 6
        self.alive = True
        self.dir = 2
        self.projectiles = []
        self.last_shot = 0
        self.cd = 10
        self.w = 11
        self.h = 14
        self.radius = (11 + 14) / 4
        self.hitbox = [0, 0, self.w, self.h]
        self.offset_x = 3
        self.offset_y = 3
        make_sensors(self, self.offset_x, self.offset_y)
        self.status = "fadein"
        self.will_teleport = 0
        self.fadeout_cd = 0
        self.fadein_cd = 15
        self.pop_cd = 0
        self.light_source = False
        self.L_radius = 0

    def teleport(self):
        self.will_teleport = 1
        self.status = "fadeout"
        self.fadeout_cd = 10

    def update(self, player, monsters, time, walls):
        if self.hp <= 0 and self.status == "attack":
            self.status = "fadeout"
            self.fadeout_cd = 10

        self.move(player, time)

        wall_collision(self, self.offset_x, self.offset_y, walls)

        if objects_overlapping(player, self):
            if self.status == "attack" and player.status == "normal":
                player.hp -= 1
                self.teleport()
                player.status = "touched"
                player.touched_cd = 20
                # print("touched")

        for proj in player.projectiles:
            if (self.status == "attack" and proj.active
                    and proj.name == "fireball"):
                col = sensor_object(self.sensors, proj)
                if col != -1:
                    self.hp -= 1
                    proj.active = 0
                    if proj.vx > 0:
                        self.vx += 1.5
                    elif proj.vx < 0:
                        self.vx -= 1.5
                    elif proj.vy > 0:
                        self.vy += 1.5
                    else:
                        self.vy -= 1.5

                    if self.hp > 0:
                        pyxel.play(0, 10)
                    else:
                        pyxel.play(1, 8)

        if self.status == "fadeout":
            self.fadeout_cd -= 1
            if self.fadeout_cd < 1:
                if self.hp > 0:
                    self.fadeout_cd = 0
                    self.status = "fadein"
                    self.fadein_cd = 15
                    if self.will_teleport == 1:
                        teleported = 0
                        while teleported == 0:
                            self.x, self.y = randint(10, 140), randint(10, 140)
                            if ((self.x - player.x)**2 +
                                    (self.y - player.y)**2 > 60**2):
                                teleported = 1
                        self.will_teleport = 0
                else:
                    self.fadeout_cd = 0
                    self.status = "pop"
                    self.pop_cd = 6

        if self.status == "fadein":
            if self.fadein_cd > 0:
                self.fadein_cd -= 1
            else:
                self.fadein_cd = 0
                self.status = "attack"

        if self.status == "pop":
            if self.pop_cd > 0:
                self.pop_cd -= 1
            else:
                self.status = "dead"
                self.alive = 0
        return self.alive

    def mutual_collide(self, monster2):
        update_sensors(self, self.offset_x, self.offset_y)
        col = sensor_sensor(self.sensors, monster2.sensors)
        if col != -1 and self.status == "attack":
            self.teleport()

        update_sensors(self, self.offset_x, self.offset_y)

    def move(self, player, time):
        if self.status == "attack":
            hyp = ((player.x - self.x)**2 + (player.y - self.y)**2)**(1 / 2)
            dx = - self.v * ((self.x - player.x) / hyp)
            dy = - self.v * ((self.y - player.y) / hyp)
            self.ax = dx / 10
            self.ay = dy / 10

            self.vx += self.ax
            self.vy += self.ay

            self.x += self.vx
            self.y += self.vy + cos(time / 10) / 4

            self.vx *= 0.9
            self.vy *= 0.9

    def draw(self, time, player):
        if (self.x - player.x) < 0:
            dir = 1
        else:
            dir = -1
        if self.status == "attack":
            pyxel.blt(self.x, self.y, 1, 64,
                      0, self.w * dir, self.h, 11)
        if self.status == "fadeout":
            pyxel.blt(self.x, self.y, 1, 80,
                      64 - (16 * int(self.fadeout_cd / 2)), 16 * dir, 16, 11)
        if self.status == "fadein":
            pyxel.blt(self.x, self.y, 1, 80,
                      (16 * int(self.fadein_cd / 2)), 16 * dir, 16, 11)
        if self.status == "pop":
            pyxel.blt(self.x, self.y, 1, 96,
                      (16 * (2 - int(self.pop_cd // 2))), 16 * dir, 16, 11)
        if DEBUGGING:
            pyxel.text(self.x + self.w, self.y + self.h, self.status, 11)
            pyxel.rectb(self.x, self.y, self.w, self.h, 3)
            pyxel.pset(*center(self), 11)
            pyxel.circ(*center(self), (self.w + self.h) / 4, 11)
            # for sensor in self.sensors:
            #     sensor.draw()
