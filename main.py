import pyxel
from random import randint, shuffle, seed
from constants import WINDOW_SIZE, TILE_SIZE, LEVEL_SIZE, SET_SEED
from entities import Level, Player, Ghost, Wall_tile, Door_tile, Torch
from menus import Main_Menu, Settings_Menu
from utilities import make_2D_array, make_object_array, center, distance_2, \
    lum_dist, reset_2D_array


class Game_Data:
    def __init__(self):
        self.win_status = 0
        self.lose_status = 0
        self.time = 0
        self.paused = 1
        self.active = 0
        self.dark_mode = 1

    def update(self):
        if self.paused == 0:
            self.time += 1


class Map:
    def __init__(self):
        self.map_array = make_2D_array(LEVEL_SIZE, LEVEL_SIZE)
        self.map_explored = make_2D_array(LEVEL_SIZE, LEVEL_SIZE)
        self.active = 0

    def update(self, level, position):
        for i in range(0, LEVEL_SIZE):
            for j in range(0, LEVEL_SIZE):
                if level.layout[i][j] == 1:
                    self.map_array[i][j] = 1
                len_monst = len(level.room[i][j].monsters)
                if len_monst > 0:
                    self.map_array[i][j] = -1 * len_monst
        self.map_explored[position[0]][position[1]] = 1

    def display(self):
        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                print(self.map_array[i, j], end="      ")
            print("")
        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                print(self.map_explored[i, j], end="     ")
            print("")


# App init

class App:
    def __init__(self):
        if SET_SEED:
            seed(42)
        pyxel.init(WINDOW_SIZE, WINDOW_SIZE, fps=30, quit_key=pyxel.KEY_F1)
        pyxel.load("assets/game_data.pyxres")

        # Create palette array (to avoid reading pyxres file everytime)
        # self.palette = []
        # for i in range(8):
        #     self.palette.append([])
        #     for j in range(16):
        #         self.palette[i].append(0)
        self.palette = make_2D_array(WINDOW_SIZE, WINDOW_SIZE)
        pyxel.blt(0, 0, 0, 0, 0, 8, 16)
        for i in range(8):
            for j in range(16):
                self.palette[i][j] = pyxel.pget(i, j)
        print("Palette :")
        for i in range(8):
            print(self.palette[i])

        self.game_data = Game_Data()

        self.current_level = 0
        self.KEYS = [[pyxel.KEY_Z, pyxel.KEY_D, pyxel.KEY_S, pyxel.KEY_Q],
                     [pyxel.KEY_W, pyxel.KEY_D, pyxel.KEY_S, pyxel.KEY_A]]
        self.ZQSD_ASWD = 0

        # Initiate menus
        self.active_menu = [0, 1, 0]  # [game, main_menu, settings_menu]
        self.main_menu = Main_Menu()
        self.main_menu.items = ["Play", "Settings", "Credits", "Exit game"]
        self.main_menu.actions = [self.main_menu.play,
                                  self.main_menu.settings,
                                  self.main_menu.credits,
                                  self.main_menu.exit]
        self.main_menu.active = 1
        self.settings_menu = Settings_Menu()
        self.settings_menu.items = ["Keyboard", "Dark mode", "Back"]
        self.settings_menu.actions = [self.settings_menu.keyboard,
                                      self.settings_menu.dark_mode,
                                      self.settings_menu.back_main_menu]
        self.main_menu.play(self.game_data)

        # Build levels until one satisfies conditions
        condition = 1
        while condition == 1:
            level_map, condition = self.build_level(LEVEL_SIZE)
        door_map = self.build_doors(level_map)
        walls = self.make_walls(door_map)
        self.level = Level(LEVEL_SIZE, level_map, door_map, walls)
        self.make_torches()

        # Initiating player
        self.player = Player()

        # Adding monsters
        self.spawn_monsters()

        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                if (len(self.level.room[i][j].monsters) == 0):
                    self.level.room[i][j].cleared = 1

        # Starting position :
        self.player.room_i = self.level.size // 2
        self.player.room_j = self.level.size // 2

        # Play music :
        # pyxel.playm(0, loop=True)

        # Initialising drawing :
        # self.lighting_mask = make_2D_array(WINDOW_SIZE, WINDOW_SIZE)
        self.lighting_mask = make_2D_array(WINDOW_SIZE, WINDOW_SIZE)

        # Building map
        self.map = Map()
        self.map.update(self.level, [self.player.room_i, self.player.room_j])

        # Run game :
        pyxel.run(self.update, self.draw)

# Build level

    def spawn_monsters(self):
        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                if self.level.layout[i][j] and not (i == j == LEVEL_SIZE // 2):
                    for k in range(randint(0, 6)):
                        self.level.room[i][j].monsters += [Ghost()]
        self.print_monster_map()

    def print_monster_map(self):
        print("Monster map :")
        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                print(len(self.level.room[i][j].monsters), end=' ')
            print("\n")

    def build_doors(self, level_map):
        door_map = []
        n = len(level_map)
        for i in range(n):
            door_map.append([])
            for j in range(n):
                door_map[i].append([0, 0, 0, 0])
        for i in range(n):
            for j in range(n):
                if level_map[i][j] == 1 and level_map[(i - 1) % n][j] == 1:
                    # print(i, j, "door up!")
                    door_map[i][j][0] = 1
                if level_map[i][j] == 1 and level_map[i][(j + 1) % n] == 1:
                    # print(i, j, "door right!")
                    door_map[i][j][1] = 1
                if level_map[i][j] == 1 and level_map[(i + 1) % n][j] == 1:
                    # print(i, j, "door down!")
                    door_map[i][j][2] = 1
                if level_map[i][j] == 1 and level_map[i][(j - 1) % n] == 1:
                    # print(i, j, "door left!")
                    door_map[i][j][3] = 1
        print("\nDoor map :")
        for i in range(LEVEL_SIZE):
            print(door_map[i])

        return door_map

    def build_level(self, n=7):
        level_map = []
        n0 = n // 2
        rooms_left = n * randint(n // 2, n * 3 // 4)
        test_static = 0

        # Initialise level
        for i in range(n):
            level_map.append([])
        for i in range(n):
            for j in range(n):
                level_map[i].append(0)
        level_map[n0][n0] = 1

        def count_neighbours(i_index, j_index):
            n_neighbours = 0
            if level_map[(i_index + 1) % n][j_index]:
                n_neighbours += 1
            if level_map[(i_index - 1) % n][j_index]:
                n_neighbours += 1
            if level_map[i_index][(j_index + 1) % n]:
                n_neighbours += 1
            if level_map[i_index][(j_index - 1) % n]:
                n_neighbours += 1
            return n_neighbours

        def add_room(i_index, j_index, rl):
            if (count_neighbours(i_index, j_index) < 2 and randint(0, 4) and
                    rl > 0 and level_map[i_index][j_index] == 0):
                level_map[i_index][j_index] = 1
                rl -= 1
            return rl

        order_i = []
        order_j = []
        for i in range(n):
            order_i.append(i)
            order_j.append(i)
        while rooms_left > 0 and test_static < 20 * n:
            shuffle(order_i)
            shuffle(order_j)
            for i in order_i:
                for j in range(n):
                    if level_map[i][j] == 1:
                        order_n = [0, 1, 2, 3]
                        shuffle(order_n)
                        for index in order_n:
                            if index == 0 and count_neighbours(i, j) < 4:
                                rooms_left = add_room((i + 1) % n, j,
                                                      rooms_left)  # Top
                            if index == 1 and count_neighbours(i, j) < 4:
                                rooms_left = add_room(i, (j + 1) % n,
                                                      rooms_left)  # Right
                            if index == 2 and count_neighbours(i, j) < 4:
                                rooms_left = add_room((i - 1) % n, j,
                                                      rooms_left)  # Bottom
                            if index == 3 and count_neighbours(i, j) < 4:
                                rooms_left = add_room(i, (j - 1) % n,
                                                      rooms_left)  # Left
            test_static += 1
        if test_static > 20 * n:
            test_static = True
        else:
            test_static = False

        print("\nMap :")
        for i in range(n):
            print(level_map[i])

        return level_map, test_static

    def make_walls(self, door_map):
        walls = make_object_array(LEVEL_SIZE, LEVEL_SIZE)
        for room_i in range(LEVEL_SIZE):
            for room_j in range(LEVEL_SIZE):
                # Go through all the wall blocks
                for i in range(0, WINDOW_SIZE + TILE_SIZE, TILE_SIZE // 2):
                    for j in range(0, WINDOW_SIZE + TILE_SIZE, TILE_SIZE // 2):
                        if i == 0:
                            if j == 0:  # upper-left corner
                                # pyxel.blt(i, j, 0, 0, 0, 16, 16, 11)
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [1, 0, 0, 1]))
                            elif j == 19 * TILE_SIZE // 2:  # bottom-left
                                # pyxel.blt(i, j, 0, 0, 0, 16, -16, 11)
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [0, 0, 1, 1]))
                            elif (door_map[room_i][room_j][3] and
                                    7 < 2 * j // TILE_SIZE < 12):
                                if 2 * j // TILE_SIZE == 8:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 3, 0))
                                if 2 * j // TILE_SIZE == 11:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 3, 3))
                            else:  # left-side wall
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [0, 0, 0, 1]))
                        elif i == 19 * TILE_SIZE // 2:
                            if j == 0:  # upper-right corner
                                # pyxel.blt(i, j, 0, 0, 0, -16, 16, 11)
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [1, 1, 0, 0]))
                            elif j == 19 * TILE_SIZE // 2:  # bottom-right
                                # pyxel.blt(i, j, 0, 0, 0, -16, -16, 11)
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [0, 1, 1, 0]))
                            elif (door_map[room_i][room_j][1] and
                                    7 < 2 * j // TILE_SIZE < 12):
                                if 2 * j // TILE_SIZE == 8:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 1, 0))
                                if 2 * j // TILE_SIZE == 11:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 1, 3))
                            else:  # right-side wall
                                # pyxel.blt(i, j, 0, 0, 16, -16, 16, 11)
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [0, 1, 0, 0]))
                        elif j == 0:  # top side wall
                            # pyxel.blt(i, j, 0, 16, 0, 16, 16, 11)
                            if (door_map[room_i][room_j][0] and
                                    7 < 2 * i // TILE_SIZE < 12):
                                if 2 * i // TILE_SIZE == 8:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 0, 0))
                                if 2 * i // TILE_SIZE == 11:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 0, 3))
                            else:
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [1, 0, 0, 0]))
                        elif j == 19 * TILE_SIZE // 2:  # bottom side wall
                            # pyxel.blt(i, j, 0, 16, 0, 16, -16, 11)
                            if (door_map[room_i][room_j][2] and
                                    7 < 2 * i // TILE_SIZE < 12):
                                if 2 * i // TILE_SIZE == 8:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 2, 0))
                                if 2 * i // TILE_SIZE == 11:
                                    walls[room_i][room_j].append(
                                        Door_tile(i, j, 2, 3))
                            else:
                                walls[room_i][room_j].append(
                                    Wall_tile(i, j, [0, 0, 1, 0]))
        return walls

    def make_torches(self):
        for i in range(LEVEL_SIZE):
            for j in range(LEVEL_SIZE):
                doors = self.level.room[i][j].doors[i][j]
                room = self.level.room[i][j]
                if doors[0]:
                    room.objects.append(Torch(WINDOW_SIZE / 2 - 14, 8))
                    room.objects.append(Torch(WINDOW_SIZE / 2 + 8, 8))
                if doors[2]:
                    room.objects.append(Torch(WINDOW_SIZE / 2 - 14,
                                              WINDOW_SIZE - 14))
                    room.objects.append(Torch(WINDOW_SIZE / 2 + 8,
                                              WINDOW_SIZE - 14))
                if doors[3]:
                    room.objects.append(Torch(8, WINDOW_SIZE / 2 - 14))
                    room.objects.append(Torch(8, WINDOW_SIZE / 2 + 8))
                if doors[1]:
                    room.objects.append(Torch(WINDOW_SIZE - 14,
                                              WINDOW_SIZE / 2 - 14))
                    room.objects.append(Torch(WINDOW_SIZE - 14,
                                              WINDOW_SIZE / 2 + 8))

# Update

    def update(self):
        self.game_data.update()
        # print(self.game_data.time, pyxel.frame_count)
        if (self.game_data.win_status or self.game_data.lose_status):
            self.game_data.paused = 1
        if pyxel.btnp(pyxel.KEY_R):
            self.player.hp = 6
            self.game_data.win_status = 0
            self.game_data.lose_status = 0
            self.player.alive = 1
        if pyxel.btnp(pyxel.KEY_U):
            self.__init__()
        if self.active_menu[1]:
            self.game_data.paused = 1
            self.update_menu(self.main_menu)
        elif self.active_menu[2]:
            self.game_data.paused = 1
            self.update_menu(self.settings_menu)
        else:
            if self.map.active:
                self.game_data.paused = 1
            else:
                self.game_data.paused = 0
            if self.game_data.paused == 0:
                self.player.update(self.game_data.time, self.KEYS,
                                   self.ZQSD_ASWD, self.level)
                self.check_exit()
                self.update_monsters()
                self.update_projectiles()
                self.update_objects()
            if pyxel.btnp(pyxel.KEY_P) or pyxel.btnp(pyxel.KEY_ESCAPE):
                self.active_menu = [0, 1, 0]
            elif pyxel.btnp(pyxel.KEY_M):
                self.map.active = 1 - self.map.active
            total_monsters = 0
            for i in range(LEVEL_SIZE):
                for j in range(LEVEL_SIZE):
                    total_monsters += len(self.level.room[i][j].monsters)
            if (total_monsters == 0):
                print("Stage clear")
                self.game_data.win_status = 1
            if self.player.hp <= 0:
                self.game_data.lose_status = 1

            # self.compute_lighting()

    def update_menu(self, menu):
        if pyxel.btnp(pyxel.KEY_UP):
            menu.cursor = (menu.cursor - 1) % len(menu.items)
        elif pyxel.btnp(pyxel.KEY_DOWN):
            menu.cursor = (menu.cursor + 1) % len(menu.items)
        elif pyxel.btnp(pyxel.KEY_RETURN):
            out = menu.actions[menu.cursor](self.game_data)
            if out is not None:
                self.active_menu = out

    def check_exit(self):
        room_i, room_j = self.player.room_i, self.player.room_j
        if (64 < self.player.x < 96 and
                self.player.y < 1 and
                self.level.doors[room_i][room_j][0] == 1):
            self.change_room(0)
        if (64 < self.player.y < 96 and
                self.player.x > 159 - self.player.w and
                self.level.doors[room_i][room_j][1] == 1):
            self.change_room(1)
        if (64 < self.player.x < 96 and
                self.player.y > 159 - self.player.h and
                self.level.doors[room_i][room_j][2] == 1):
            self.change_room(2)
        if (64 < self.player.y < 96 and
                self.player.x < 1 and
                self.level.doors[room_i][room_j][3] == 1):
            self.change_room(3)

    def change_room(self, door):
        if door == 0:
            self.player.room_i = (self.player.room_i - 1) % LEVEL_SIZE
            self.player.y = 159 - self.player.h
        if door == 1:
            self.player.room_j = (self.player.room_j + 1) % LEVEL_SIZE
            self.player.x = 1
        if door == 2:
            self.player.room_i = (self.player.room_i + 1) % LEVEL_SIZE
            self.player.y = 1
        if door == 3:
            self.player.room_j = (self.player.room_j - 1) % LEVEL_SIZE
            self.player.x = 159 - self.player.w
        print("Now in", self.player.room_i, self.player.room_j)
        self.print_monster_map()
        self.player.projectiles = []
        self.map.update(self.level, [self.player.room_i, self.player.room_j])
        # self.map.display()

    def update_monsters(self):
        i_room, j_room = self.player.room_i, self.player.room_j
        monsters = self.level.room[i_room][j_room].monsters
        n_monst = len(monsters)

        still_alive = []
        for i in range(n_monst):
            still_alive.append(1)
        for i, monster in enumerate(monsters):
            still_alive[i] = monster.update(
                self.player, monsters, self.game_data.time,
                self.level.room[i_room][j_room].walls)
            for j in range(i, n_monst):
                if still_alive[i] and still_alive[j]:
                    monster.mutual_collide(monsters[j])

        i = 0
        while i < n_monst:
            if still_alive[i] == 0:
                monsters.pop(i)
                still_alive.pop(i)
                self.player.kills += 1
                if (self.player.kills % 3) == 0:
                    self.player.heal(1)
                i = -1
                n_monst = len(monsters)
            i += 1

        if (self.level.room[self.player.room_i][self.player.room_j].cleared
                == 0 and n_monst == 0):
            self.level.room[self.player.room_i][self.player.room_j].cleared = 1
            pyxel.play(0, 7)

    def update_projectiles(self):
        room_i, room_j = self.player.room_i, self.player.room_j
        for i, proj in enumerate(self.player.projectiles):
            proj.update(self.level.room[room_i][room_j])
            if self.player.projectiles[i].active == 0:
                self.player.projectiles.pop(i)

    def update_objects(self):
        room = self.get_player_location()
        for i, object in enumerate(room.objects):
            object.update()

# Draw

    def draw(self):

        if self.active_menu[1]:
            self.draw_menu(self.main_menu)
        elif self.active_menu[2]:
            self.draw_menu(self.settings_menu)
        # Background
        else:
            pyxel.cls(3)
            self.draw_floor()
            # self.draw_projectiles()
            # if self.player.alive:

            self.player.draw(self.game_data.time)
            for i, monster in enumerate(
                self.level.room[self.player.room_i]
                               [self.player.room_j].monsters):
                monster.draw(self.game_data.time, self.player)
            # self.draw_doors()
            # self.draw_monsters()

            self.draw_walls()

            if self.game_data.dark_mode:
                for object in self.get_player_location().objects:
                    object.draw()

                self.apply_lighting()

            for proj in self.player.projectiles:
                proj.draw(self.game_data.time)

            # self.compute_lighting()
            # self.draw_lighting()

            self.draw_GUI()
            self.draw_map()

    def compute_lighting(self):
        # Fetching all light sources
        room = self.get_player_location()
        entities = []
        for monster in room.monsters:
            if monster.light_source:
                entities.append(monster)
        for object in room.objects:
            if object.light_source:
                entities.append(object)
        for projectile in self.player.projectiles:
            if projectile.name == "soundwave":
                for wavepart in projectile.wave_particles:
                    if wavepart.light_source:
                        entities.append(wavepart)
            else:
                if projectile.light_source:
                    entities.append(projectile)
        entities.append(self.player)

        # reset_2D_array(self.lighting_mask)
        self.lighting_mask = make_2D_array(WINDOW_SIZE, WINDOW_SIZE)

        for e, entity in enumerate(entities):
            x, y = center(entity)
            x, y = round(x), round(y)
            x_min = round(max(0, x - entity.L_radius))
            x_max = round(min(WINDOW_SIZE, x + entity.L_radius))
            y_min = round(max(0, y - entity.L_radius))
            y_max = round(min(WINDOW_SIZE, y + entity.L_radius))

            for i in range(x_min, x_max):
                for j in range(y_min, y_max):
                    if (distance_2(i, j, *center(entity)) < entity.L_radius):
                        ldist = lum_dist(i, j, round(x),
                                         round(y), entity.L_radius)
                        self.lighting_mask[i][j] += ldist

    def draw_lighting(self):
        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                lum = min(self.lighting_mask[i][j], 0.99999999999)
                if (lum != 0):
                    lum_color = 0
                    check = 0
                    for n_col in range(8):
                        if (n_col / 8 < lum < (n_col + 1) / 8):
                            lum_color = n_col
                            check += 1
                    if (check > 1):
                        print(f"Warning: non unique lum for pixel ({i}, {j})")
                        print(f"lum ={lum}")
                    ori_col = pyxel.pget(i, j)
                    pyxel.pset(i, j, self.palette[lum_color][ori_col])
                else:
                    pyxel.pset(i, j, 0)

    def apply_lighting(self):
        room = self.get_player_location()
        entities = []
        for monster in room.monsters:
            if monster.light_source:
                entities.append(monster)
        for object in room.objects:
            if object.light_source:
                entities.append(object)
        for projectile in self.player.projectiles:
            if projectile.name == "soundwave":
                for wavepart in projectile.wave_particles:
                    if wavepart.light_source:
                        entities.append(wavepart)
            else:
                if projectile.light_source:
                    entities.append(projectile)
        entities.append(self.player)
        # print("entity number :", len(entities))
        if (pyxel.frame_count % 2) == 0:
            reset_2D_array(self.lighting_mask)

            for e, entity in enumerate(entities):
                x, y = center(entity)
                x, y = round(x), round(y)
                x_min = round(max(0, x - entity.L_radius))
                x_max = round(min(WINDOW_SIZE, x + entity.L_radius))
                y_min = round(max(0, y - entity.L_radius))
                y_max = round(min(WINDOW_SIZE, y + entity.L_radius))

                for i in range(x_min, x_max):
                    for j in range(y_min, y_max):
                        if (distance_2(i, j, *center(entity)) <
                                entity.L_radius):
                            ldist = lum_dist(i, j, round(x),
                                             round(y), entity.L_radius)
                            self.lighting_mask[i][j] += ldist

        for i in range(WINDOW_SIZE):
            for j in range(WINDOW_SIZE):
                lum = min(self.lighting_mask[i][j], 0.99999999999)
                if (lum != 0):
                    lum_color = 0
                    check = 0
                    for n_col in range(8):
                        if (n_col / 8 < lum < (n_col + 1) / 8):
                            lum_color = n_col
                            check += 1
                    pyxel.pset(i, j, self.palette[lum_color][pyxel.pget(i, j)])
                else:
                    pyxel.pset(i, j, 0)

    def draw_menu(self, menu):
        pyxel.cls(menu.bg_color)
        n_items = len(menu.items)
        for i, option in enumerate(menu.items):
            x = pyxel.width * 1 / 3
            y = 2 * TILE_SIZE + (i / n_items) * (pyxel.height - 2 * TILE_SIZE)
            if i == menu.cursor:
                pyxel.text(x, y, option, menu.button_on)
            else:
                pyxel.text(x, y, option, menu.button_off)

    def draw_GUI(self):
        pyxel.text(11 + 1, 11 + 1, "HP : " + str(self.player.hp), 1)
        pyxel.text(11 + 1, 18 + 1, "Score : " + str(self.player.kills), 1)
        pyxel.text(11, 11, "HP : " + str(self.player.hp), 8)
        pyxel.text(11, 18, "Score : " + str(self.player.kills), 8)
        if self.player.alive == 0:
            pyxel.text(60, 25 + 35, "GAME OVER", 8)
            pyxel.text(60, 25 + 55, "Score : " + str(self.player.kills), 8)

    def draw_floor(self):
        for i in range(0, WINDOW_SIZE, 2 * TILE_SIZE):
            for j in range(0, WINDOW_SIZE, 2 * TILE_SIZE):
                pyxel.blt(i, j, 0, 64, 48, 32, 32, 11)

    def draw_walls(self):
        for wall in self.level.room[self.player.room_i][
                self.player.room_j].walls:
            # print(wall)
            wall.draw()

    def draw_doors(self):
        if self.level.doors[self.player.room_i][self.player.room_j][0]:
            pyxel.blt(4 * TILE_SIZE, 0 * TILE_SIZE, 0, 48, 0, 32, 16, 11)

        if self.level.doors[self.player.room_i][self.player.room_j][1]:
            pyxel.blt(9 * TILE_SIZE, 4 * TILE_SIZE, 0, 32, 0, -16, 32, 11)

        if self.level.doors[self.player.room_i][self.player.room_j][2]:
            pyxel.blt(4 * TILE_SIZE, 9 * TILE_SIZE, 0, 48, 0, 32, -16, 11)

        if self.level.doors[self.player.room_i][self.player.room_j][3]:
            pyxel.blt(0 * TILE_SIZE, 4 * TILE_SIZE, 0, 32, 0, 16, 32, 11)

    def draw_map(self):
        if self.map.active:
            len = 100
            offset = 30
            pyxel.rect(offset - 2, offset - 2, len + 4, len + 4, 7)
            pyxel.rect(offset, offset, len, len, 0)
            r_size = len // LEVEL_SIZE - 2
            rest = len - r_size * LEVEL_SIZE
            colors = []
            min = 0
            for i in range(LEVEL_SIZE):
                for j in range(LEVEL_SIZE):
                    if self.map.map_array[i][j] < min:
                        min = self.map.map_array[i][j]
                    if self.map.map_array[i][j] != 0:
                        x = offset + rest / 2 + i * r_size
                        y = offset + rest / 2 + j * r_size
                        pyxel.rect(x, y, r_size, r_size, 0)
                        pyxel.rect(x + 1, y + 1, r_size - 2, r_size - 2, 1)

            for i in range(3):
                for j in range(int(abs(min) // 3)):
                    colors += [10 - i]
                    if i == 1 and j == 0:
                        for k in range(int(abs(min) % 3)):
                            colors += [9]

            for i in range(LEVEL_SIZE):
                for j in range(LEVEL_SIZE):
                    x = offset + rest / 2 + j * r_size
                    y = offset + rest / 2 + i * r_size
                    col = colors[abs(int(self.map.map_array[i][j])) - 1]
                    if self.map.map_array[i][j] < 0:
                        pyxel.rect(1 + x, y + 1, r_size - 2, r_size - 2, col)
                    if i == self.player.room_i and j == self.player.room_j:
                        pyxel.blt(1 + x, y + 1, 1, 0, 80, 8, 8, 11)

# Other

    def get_player_coordinates(self):
        return self.player.room_i, self.player.room_j

    def get_player_location(self):
        return self.level.room[self.player.room_i][self.player.room_j]


App()
