# Rogueweek

Game started in a week long game jam. I didn't have time back then and continued after that. I will not continue developping this game.

![presentation](screenshots/rogue_showcase.gif)

# Requirements
- python3
- pyxel library version 1.3 or higher

# Run

In terminal :
```sh
python3 main.py
```

For linux:
```sh
./dist/linux/pyxel_rogue
```

# How to play

- Move using arrow keys
- Switch character with spacebar
- Shoot using ASWD or ZQSD
- Pause with P
- Open map with M (WIP)
- Dark mode (shaders) can be toggled in settings
