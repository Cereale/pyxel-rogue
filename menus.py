import pyxel


class Main_Menu:
    def __init__(self):
        self.cursor = 0
        self.items = []
        self.actions = []
        self.bg_color = 0
        self.button_off = 2
        self.button_on = 8
        self.active = 0

    def play(self, game_data):
        return [1, 0, 0]

    def settings(self, game_data):
        return [0, 0, 1]

    def credits(self, game_data):
        print("Made by Antoine")
        print("Thank you for playing !")
        return None

    def exit(self, game_data):
        pyxel.quit()


class Settings_Menu:
    def __init__(self):
        self.cursor = 0
        self.items = []
        self.actions = []
        self.bg_color = 0
        self.button_off = 2
        self.button_on = 8
        self.active = 0
        self.volume = 5
        self.keyboard_setting = 1   # 1 = AZERTY, 0 = QWERTY

    def keyboard(self, game_data):
        self.keyboard_setting = not self.keyboard_setting
        return None

    def back_main_menu(self, game_data):
        return [0, 1, 0]

    def dark_mode(self, game_data):
        game_data.dark_mode = not game_data.dark_mode
        print("dark mode ", end="")
        if game_data.dark_mode:
            print("on")
        else:
            print("off")
        return None
